import { getMobileOS } from './Utils';
import Socket from './Socket';

const DEV_PORT = '8080'; // TODO: get from env vars

export default class Controls {
  constructor() {
    this.enableOrientation = this.enableOrientation.bind(this);
    this.deviceOrientation = this.deviceOrientation.bind(this);
    this.orientationChange = this.orientationChange.bind(this);
    this.deviceMotion = this.deviceMotion.bind(this);
    this.checkForEnterPressed = this.checkForEnterPressed.bind(this);
    this.connect = this.connect.bind(this);

    this.state = {
      screen: null,
    };

    this.element = document.createElement('div');
    this.element.className = 'controls';

    this.debug = document.createElement('p');
    this.debug.className = 'controls-debug';

    this.background = document.createElement('div');
    this.background.className = 'controls-background';

    this.enableButton = document.createElement('button');
    this.enableButton.className = 'controls-enable hidden';
    this.enableButton.innerText = 'Enable Gyroscope';

    this.code = document.createElement('div');
    this.code.className = 'controls-code';

    this.codeDescription = document.createElement('p');
    this.codeDescription.className = 'controls-code-description';
    this.codeDescription.innerText = 'Enter screen code to pair';

    this.codeInput = document.createElement('input');
    this.codeInput.className = 'controls-code-input';
    this.codeInput.placeholder = 'CODE';
    this.codeInput.type = 'text';
    this.codeInput.setAttribute('maxlength', 4);
    this.codeInput.setAttribute('spellcheck', 'false');

    this.codeHint = document.createElement('p');
    this.codeHint.className = 'controls-code-hint';
    this.codeHint.innerHTML = 'No Code?<br>Visit rx-co.de/phone-cursor on your PC.';

    this.code.appendChild(this.codeDescription);
    this.code.appendChild(this.codeInput);
    this.code.appendChild(this.codeHint);

    this.element.appendChild(this.background);
    this.element.appendChild(this.code);
    this.element.appendChild(this.enableButton);
    this.element.appendChild(this.debug);

    this.enableButton.addEventListener('click', () => {
      this.enableMotion();
      this.enableOrientation();
    });
    this.codeInput.addEventListener('blur', this.connect);
    this.codeInput.addEventListener('keydown', this.checkForEnterPressed);

    const { port, pathname } = window.location;
    if (port !== DEV_PORT) { // Not checking for code in URL on webpack-dev-server yet
      const codeInURL = pathname.split('/phone-cursor/')[1].replace(/[^A-Za-z]/g, '');
      if (codeInURL.length >= 4) {
        this.codeInput.value = codeInURL.substring(0, 4);
        this.connect();
      }
    }
  }

  checkForEnterPressed({ key }) {
    if (key === "Enter") this.connect();
    setTimeout(() => {
      if (this.codeInput.value.length === 4) this.connect();
    }, 100);
  }

  connect() {
    if (this.codeInput.value.length < 4) return;

    this.socket = new Socket('controls', this.codeInput.value, getMobileOS());

    this.socket.on('screenFound', ({ result }) => {
      if (result) {
        this.state.screen = result.screen;
        this.codeInput.removeEventListener('blur', this.connect);
        this.codeInput.blur();
        this.enableButton.focus();
        this.enableButton.classList.remove('hidden');
        this.code.classList.add('hidden');
        window.navigator.vibrate(500);
      } else {
        this.codeInput.classList.add('error');
      }
    });
  }

  enableOrientation() {
    if (typeof DeviceOrientationEvent.requestPermission === 'function') {
      DeviceOrientationEvent.requestPermission().then((permissionState) => {
        if (permissionState === 'granted') {
          window.addEventListener('deviceorientation', this.deviceOrientation);
          window.addEventListener("orientationchange", this.orientationChange);
          this.enableButton.classList.add('hidden');
        }
      }).catch(console.error);
    } else {
      window.addEventListener('deviceorientation', this.deviceOrientation);
      window.addEventListener('orientationchange', this.orientationChange);
      this.enableButton.classList.add('hidden');
    }
  }

  enableMotion() {
    if (typeof DeviceMotionEvent.requestPermission === 'function') {
      DeviceMotionEvent.requestPermission().then((permissionState) => {
        if (permissionState === 'granted') {
          window.addEventListener('devicemotion', this.deviceMotion);
          this.enableButton.classList.add('hidden');
        }
      }).catch(console.error);
    } else {
      window.addEventListener('devicemotion', this.deviceMotion);
      this.enableButton.classList.add('hidden');
    }
  }


  orientationChange() {
    this.socket.emit('orientationChange', { screen: this.state.screen, orientation: window.orientation });
  }

  deviceOrientation({ alpha, beta, gamma }) {
    this.socket.emit('orientation', { screen: this.state.screen, alpha, beta, gamma });
  }

  deviceMotion({ acceleration }) {
    const { x, y, z } = acceleration;
    this.socket.emit('motion', { screen: this.state.screen, x, y, z });
  }
}
