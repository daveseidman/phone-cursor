import io from 'socket.io-client';

export default class Socket {
  constructor(type, code, os) {
    const socket = io(`?type=${type}${code ? `&code=${code}` : ``}&os=${os}`);
    return socket;
  }
}
