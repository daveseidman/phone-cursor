import { degToRad } from './Utils';

import Socket from './Socket';


const urlParams = new URLSearchParams(window.location.search);
const testingControls = urlParams.has('testcontrols');

export default class Screen {
  constructor() {
    this.update = this.update.bind(this);
    this.showRoomCode = this.showRoomCode.bind(this);
    this.controlsFound = this.controlsFound.bind(this);
    this.hideConnect = this.hideConnect.bind(this);
    this.orientation = this.orientation.bind(this);
    this.motion = this.motion.bind(this);

    this.state = {
      connected: false,
      permission: false,
      steeringCentered: false, // TODO: implement this, user must hold rotation close to 0 for a second before starting
    };

    this.acceleration = { x: 0, y: 0, z: 0 };

    this.socket = new Socket('screen');
    this.socket.on('roomCode', this.showRoomCode);
    this.socket.on('controlsFound', this.controlsFound);
    this.socket.on('orientationChange', ({ orientation }) => { this.orient = orientation; });
    this.socket.on('orientation', this.orientation);
    this.socket.on('motion', this.motion);

    this.element = document.createElement('div');
    this.element.className = 'screen';

    this.connect = document.createElement('div');
    this.connect.className = `screen-connect ${testingControls ? 'hidden' : ''}`;

    this.connectCodeDescription = document.createElement('p');
    this.connectCodeDescription.className = 'screen-connect-code-description';
    this.connectCodeDescription.innerHTML = 'visit rx-co.de/phone-cursor on your mobile device<br>and enter this code';

    this.connectCode = document.createElement('div');
    this.connectCode.className = 'screen-connect-code';

    this.connectImageDescription = document.createElement('p');
    this.connectImageDescription.className = 'screen-connect-image-description';
    this.connectImageDescription.innerText = 'or scan the image below';

    this.connectImage = document.createElement('img');
    this.connectImage.className = 'screen-connect-image';

    this.connect.appendChild(this.connectCodeDescription);
    this.connect.appendChild(this.connectCode);
    this.connect.appendChild(this.connectImageDescription);
    this.connect.appendChild(this.connectImage);

    this.connected = document.createElement('div');
    this.connected.className = 'screen-connected hidden';
    this.connected.innerHTML = '<p>You are now connected<br>Click ENABLE GYROSCOPE to continue</p>';

    this.element.appendChild(this.connect);
    this.element.appendChild(this.connected);

    this.orient = 0;

    this.debug = document.createElement('div');
    this.debug.className = 'screen-debug';
    document.body.appendChild(this.debug);

    this.update();
    window.addEventListener('resize', this.resize);
    window.addEventListener('keydown', this.keydown);
    window.addEventListener('keyup', this.keyup);
  }

  showRoomCode({ code }) {
    this.connectCode.innerText = code;
    this.connectImage.src = `https://api.qrserver.com/v1/create-qr-code/?size=600x600&data=http://rx-co.de/pilot/${code}`;
  }

  controlsFound({ os }) {
    this.controllerOS = os;
    console.log('connected to an', os);
    if (this.state.permission) return;
    this.connect.classList.add('hidden');
    this.connected.classList.remove('hidden');
    this.state.connected = true;
  }

  hideConnect() {
    this.connected.classList.add('hidden');
  }

  update() {
    if (this.state.permission) {
      // this.debug.innerHTML = `${this.orient}<br>${rot.x.toFixed(1)}, ${rot.y.toFixed(1)}, ${rot.z.toFixed(1)}`;
    }

    requestAnimationFrame(this.update);
  }

  orientation({ alpha, beta, gamma }) {
    if (!this.state.permission) {
      this.state.permission = true;
      this.hideConnect();
    }

    this.debug.innerHTML = `<p>${Math.round(alpha)}</p><p>${Math.round(beta)}</p><p>${Math.round(gamma)}</p>`;
  }

  motion({ x, y, z }) {
    if (!this.state.permission) {
      this.state.permission = true;
      this.hideConnect();
    }
  }
}
