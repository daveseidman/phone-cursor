require('dotenv').config();

const express = require('express');
const fs = require('fs');
const https = require('https');
const http = require('http');
const chalk = require('chalk');

const app = express();

const port = 8000;
const rooms = {};


// remote server is behind actual SSL
// local uses express self signed cert
const server = process.env.SSL === 'true'
  ? http.createServer(app)
  : https.createServer({ key: fs.readFileSync('server/server.key'), cert: fs.readFileSync('server/server.cert') }, app);

const io = require('socket.io').listen(server);

server.listen(port, () => {
  console.log(`phone-cursor server listening on ${port}`);
});

app.use('/pilot', express.static(`${__dirname}/../dist`)); // TODO: can probably use a wildcard here
app.use('/pilot/:code', express.static(`${__dirname}/../dist`));


const randomCode = () => [...Array(4)].map(() => String.fromCharCode(65 + Math.floor(Math.random() * 26))).join('');

const codeIsUnique = (code) => {
  let unique = true;
  for (let i = 0; i < Object.keys(rooms).length; i += 1) {
    if (code === rooms[Object.keys(rooms)[i]].code) unique = false;
    break;
  }
  return unique;
};


const createRoom = (socket) => {
  let code = randomCode();
  while (!codeIsUnique) code = randomCode();
  rooms[socket] = {
    code,
    screen: socket,
    controls: null,
    controllerOS: null,
    status: 'waiting',
  };
  return code;
};

const getRoomByCode = (code) => {
  let matchingRoom = null;
  const keys = Object.keys(rooms);
  for (let i = 0; i < keys.length; i += 1) {
    if (rooms[keys[i]].code.toLowerCase() === code.toLowerCase()) {
      matchingRoom = rooms[keys[i]];
      break;
    }
  }
  return matchingRoom;
};

const getTypeBySocket = (socketID) => {
  // the default type is controls because if the screen leaves before the controls do
  // the room is automatically deleted
  let type = 'controls';
  const keys = Object.keys(rooms);
  for (let i = 0; i < keys.length; i += 1) {
    if (socketID === rooms[keys[i]].controls) {
      type = 'controls';
      break;
    }
    if (socketID === rooms[keys[i]].screen) {
      type = 'screen';
      break;
    }
  }
  return type;
};

const getRoomBySocket = (socketID) => {
  let room = null;
  const keys = Object.keys(rooms);
  for (let i = 0; i < keys.length; i += 1) {
    if (socketID === rooms[keys[i]].screen || socketID === rooms[keys[i]].controls) {
      room = rooms[keys[i]];
      break;
    }
  }
  return room;
};


io.on('connection', (socket) => {
  const { query } = socket.handshake;

  if (query.type === 'screen') {
    const code = createRoom(socket.id);
    socket.emit('roomCode', { code });
    console.log(chalk.green('screen connected:   ', code));
  }


  if (query.type === 'controls') {
    const { os, code } = query;
    const matchingRoom = getRoomByCode(code);
    if (matchingRoom) {
      console.log(chalk.green('controls connected:   ', code.toUpperCase()));
      matchingRoom.controls = socket.id;
      io.to(matchingRoom.screen).emit('controlsFound', { os });
    }

    socket.emit('screenFound', { result: matchingRoom });

    socket.on('orientationChange', ({ screen, orientation }) => {
      io.to(screen).emit('orientationChange', { orientation });
    });

    socket.on('orientation', ({ screen, alpha, beta, gamma }) => {
      io.to(screen).emit('orientation', { alpha, beta, gamma });
    });

    socket.on('motion', ({ screen, x, y, z }) => {
      io.to(screen).emit('motion', { x, y, z });
    });
  }


  socket.on('disconnect', () => {
    const type = getTypeBySocket(socket.id);
    console.log(chalk.red(`${type} disconnected: ${getRoomBySocket(socket.id) ? getRoomBySocket(socket.id).code : ''}`));
    // TODO: send signals to controller or screen if it's companion disconnected
    if (type === 'screen') delete (rooms[socket.id]);
  });

  console.log('there are', Object.keys(rooms).length, 'rooms');
});
